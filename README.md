# ArgoCD
O ArgoCD é uma ferramenta GitOps de Continous Delivery (CD) para Kubernetes focada na gestão dos deployments.
Permite que as definições, configurações e ambientes das aplicações sejam controladas por versão, de modo que a implemantação das aplicações sejam automatizadas e fáceis de analisar.

Oferece um conjunto de recursos tais como: várias opções de sincronização, controlo de acessos personalizado e verificação do estado da apliacação, etc.

As indicações referidas na presente **Documentação** já foram efetuadas.

## Passos para Instalação

#### 1.Namespace

Criar namespace no qual se vai fazer a instalação do cluster:

```
kubectl create namespace argocd
```

`NOTA:` A imagem do ArgoCD, neste momento já se encontra no Repositório HARBOR, desta forma o passo número `2.Secret` já não é necessário efetuar.

#### 2.Secret
Criar o secret que é utilizado para se ter acesso ao Nexus. As credenciais podem ser obtidas da seguinte forma:

O **user** utilizado foi o seguinte: 

`USER NEXUS`  
 **user:** docker-kubernetes  


```
cat ~/.docker/config.json | base64
```

O secret deve ser feito a partir de um yaml:

```
vim secret.yaml
```
Ter em atenção que as credenciais em base64 devem ser colocadas numa só linha (pode ser necessário retirar os “ENTER” entre as linhas das credenciais).

```
apiVersion: v1
kind: Secret
metadata:
  name: <nome do secret, ex: "secret-argocd">
  namespace: <namespace onde secret pode ser usado, ex: "argocd">
data:
  .dockerconfigjson: <credenciais base64>
type: kubernetes.io/dockerconfigjson
```

Criar o secret e fazer patch para o service account default:

```
kubectl -n argocd apply -f secret.yaml
kubectl -n argocd patch serviceaccount default -p '{"imagePullSecrets": [{"name":"secret-argocd"}]}'
```

Caso se crie pods de forma manual, verificar se tem algum service account atribuído (caso não tenha, utiliza o default). Se tiver algum atribuído, pode ser necessário fazer patch para esse service account de forma a utilizar o secret.

#### 3.Deploy ArgoCD

Nesta etapa, colaca-se o ArgoCD no cluster, mas antes terão de ser feitas algumas alterações e verifações aos ficheiros ".yaml":

```
cd argocd/
```

Os ficheiros devem ser adaptados para as necessidades (estas alterações já estão feitas para a instalação):

* **Ficheiro Deployment** - Colocar o nome da imagem correto;

```
Exemplo:
image: registry.dev.grupocgd.com/cgd/argoproj/argocd-ca:v1.7.8
image: registry.dev.grupocgd.com/cgd/dexidp/dex:v2.22.0
image: registry.dev.grupocgd.com/cgd/redis:5.0.8
```

`NOTA:` É apenas necessário configurar o **imagePullSecrets** se o repositório for o **Nexus**.

* **Ficheiro ServiceAccount** - Adicionar `imagePullSecrets`, o que permite que o secret criado anteriormente seja adicionado a todos os service account criados;

```
Exemplo:
imagePullSecrets:
- name: <nome do secret, ex: "secret-argocd">
```

Correr o seguinte comando:
```
kubectl apply -n argocd -f argocd
````

Verificar criação dos pods, serão criados 5 pods com os seguintes nomes (`argocd-application-controller`, `argocd-dex-server`, `argocd-redis`,  	`argocd-repo-server`, `argocd-server` ):

```
kubectl -n argocd get pods
````
- `CRIAR O NAMESPACE NO CLUSTER PARA O QUAL QUEREMOS FAZER DEPLOY DA APP`;

De forma a criar os **Projetos** no ArgoCD é necessário criar os ficheiros **.yaml**, colocar na pasta **Project** e correr o seguinte comando:

É **OBRIGATÓRIO** criar primeiro os projetos, antes de criar as aplicações.

Encontra-se um **template e um exemplo** na pasta Exemplos App e Project.

```
kubectl apply -n argocd -f Project
````

De forma a criar as **Aplicações** no ArgoCD é necessário criar os ficheiros **.yaml**, colocar na pasta **Deploy** e correr o seguinte comando:

Encontra-se um **template e um exemplo** na pasta Exemplos App e Project.

```
kubectl apply -n argocd -f Deploy
````

Também é possivel criar os projetos e as aplicações via **interface** mas se ocorrer alguma anomalia no ArgoCD, as alterações feitas pela interface não serão guardadas para o futuro.

#### 4.Dashboard

##### Fazer ingress para Dashboard

Através do `Rancher Resources` -> `Workloads` -> `Load Balancing` -> `Add ingress`:

- Dar nome que se quer ao ingress
- Adicionar namespace específico do serviço
- Dar host específico (ex: argocd.cloudlab.k8scgd.com)
- Adicionar regra (Target Backend) de **Service**: Target é o serviço - **argocd-server** com o porto **https**.

A criação de um Ingress também pode ser feita via ficheiro yaml, segue-se o ficheiro, apenas é necessário alterar o campo **spec.rules.host.**

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: argocd-server-http-ingress
  namespace: argocd
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - host: argocd.k8s1.grupocgd.com
    http:
      paths:
      - path: /
        backend:
          serviceName: argocd-server
          servicePort: http
    
```


##### Ligação ao Keycloack

A ferramenta ArgoCD já está conectada com Keyloack mas deixo aqui um link com os passos todos a seguir:

**Link:** https://argoproj.github.io/argo-cd/operator-manual/user-management/keycloak/

- É necessário adicionar o certficado `ca-cgd.crt` à imagem do ArgoCD que se encontra no Nexus. ( Imagem em utilização: `cgd/argoproj/argocd:v1.5.0`)
- No momento de **Configuring the groups claim** -> **Add Client Scope**, o nome que se atribui ao **Client Scope** tem de ser - *OBRIGATORIAMENTE* - `groups`.
- Não é necessário criar o grupo "ArgoCDAdmins", visto que já temos grupos criados para os utilizadores. 
- Fazer as alterações indicadas no link no ficheiro do **argocd-secret** e nos **ConfigMap**.

#### 5.Monitorização com Prometheus e Grafana

Este procedimento assume que o cluster já tem instâncias de Prometheus e Grafana a monitorizar o mesmo. Primeiramente deve-se verificar se o Prometheus está apenas a monitorizar os namespaces do seu projeto no Rancher, caso esteja, deve-se retirar os namespaces que lá estão especificados, assim começa a observar todo o cluster.

De forma a ativar a monitorização por parte do **Prometheus** deve-se acrescentar as seguintes anotações:


* **Ficheiro Deployment.yml** - Colocar as seguintes anotações nos pods **argocd-application-controller**, **argocd-repo-server** e **argocd-server**, em `spec.template.metadata.annotations`:
```
annotations:
        prometheus.io/path: /metrics
        prometheus.io/port: "PORT" 
        prometheus.io/scrape: "true"
````
O `PORT` é diferente em cada pod.


Relativamente ao **Grafana**, é necessário copiar o ficheiro **dashboard.json** do link abaixo indicado e colar no Grafana ( `Create -> Import`).

- https://github.com/argoproj/argo-cd/blob/master/examples/dashboard.json

De seguida, já pode ser consultado no Grafana o dashboard correspondente ao ArgoCD.

#### 6. Configuração de um Cluster Externo

Através do seguinte ficheiro efectua-se a comunicação com um cluster externo, permitindo assim fazer-se deploy não só para o cluster onde o ArgoCD está instalado mas também para outro(s) cluster, assim ficamos apenas com um ArgoCD Central.

Neste ficheiro existem 3 campos fundamentais **(server, bearerToken, caData)** a preencher com informações relativas ao cluster externo:

```
apiVersion: v1
kind: Secret
metadata:
  name: mycluster-secret
  labels:
    argocd.argoproj.io/secret-type: cluster
type: Opaque
stringData:
  name: cluster-lab-upgrade
  server: https://rancher.cqgrupocgd.com/k8s/clusters/c-58xl55
  config: |
    {
      "bearerToken": "token-gbhhq:dd7bnmjxkqf74z659s5w5vsl2dzwpp98dfg2z45r4fv57p252vvwrgw",
      "tlsClientConfig": {
        "insecure": false,
        "caData": "<Encontra-se no Kubeconfig do cluster>"
      }
    }
```

A informação relativa aos campos **server** e **caData** encontram-se no Kubeconfig do cluster externo. Relativamente ao campo **bearerToken** é necessário criar uma `KEY` no rancher, clicando em `API & Keys`.

Por fim, é necessário fazer o comando `kubectl apply -n argocd -f ficheiro-cluster-secret.yaml`.

#### 7.Eliminação do ArgoCD

De modo a apagar o ArgoCD do cluster, seguir os seguintes passos:

```
kubectl delete -n argocd -f argocd
````

Confirmar se não ficaram `service accounts`, `resource definitions`, `projects` ou `applications`:

```
kubectl -n argocd get sa
kubectl -n argocd get crd
kubectl -n argocd get appproject
kubectl -n argocd get application
````


